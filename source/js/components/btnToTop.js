class BtnToTop {
	constructor(element, trigger = 200) {
		this.element = element;
		this.trigger = trigger;

		this.init();
	}

	init() {
		this.handlerClick();
		this.handlerScroll();
	}

	handlerClick() {

		this.element.addEventListener("click", (e) =>  {
			e.preventDefault();
			this.moveToTop();
		});

	}

	moveToTop() {
		
		if( window.scrollY != 0 ) {
			setTimeout( () => {
				window.scrollTo( 0, window.scrollY - 30 );
				this.moveToTop();
			}, 5);
		}

	}

	show() {
		this.element.classList.add("btn-to-top_show");
	}

	hidden() {
		this.element.classList.remove("btn-to-top_show");
	}

	handlerScroll() {
		window.addEventListener("scroll", () => {
			if (window.scrollY >= this.trigger) {
				this.show();
			} else {
				this.hidden();
			}
		});

	}
}