class Menu {
	constructor(btn, blockMenu) {

		this.bodyPage           = document.querySelector("body");
		this.siteMenu           = blockMenu;
		this.burgerBtn 			= btn;
		this.contentSiteMenu 	= this.siteMenu.querySelector(".menu__content");
		this.closeSiteMenu 		= this.siteMenu.querySelector(".menu__close");

		this.init();

	}

	init() {
		this.handlerClickBurger();
		this.handlerCloseMenu();
	}

	// клик по бургеру
	handlerClickBurger() {
		
		this.burgerBtn.addEventListener("click", () => {
			this.bodyPage.style.overflow = "hidden";
			this.siteMenu.classList.add("menu_active");
		});

	}

	
	handlerCloseMenu() {

		// закрытие по клику на пустое место в меню
		this.contentSiteMenu.addEventListener("click", (e) => {
			e.stopPropagation();
		});

		this.siteMenu.addEventListener("click",  (e) => {
			e.stopPropagation();
			e.preventDefault();
			this.siteMenu.classList.remove("menu_active");
			this.bodyPage.style.overflow = "auto";
		});
		
		// закрытие по клику на крестик
		this.closeSiteMenu.addEventListener("click", (e) => {
			e.stopPropagation();
			e.preventDefault();
			this.siteMenu.classList.remove("menu_active");
			this.bodyPage.style.overflow = "auto";
		});

	}
	

	
}