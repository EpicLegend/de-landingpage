class ServiceItem {
	constructor(element) {
		this.element = element;
		this.range = 40;

		this.handlerMouseMove();
	}

	handlerMouseMove() {
		
		let timeout;
		const calcValue = (a, b) => (a / b * this.range - this.range / 2).toFixed(1);

		this.element.addEventListener('mousemove', ( {x, y} ) => {
			
			if (timeout) {
				window.cancelAnimationFrame(timeout);
			}

			timeout = window.requestAnimationFrame(() => {
				const yValue = calcValue(y, window.innerHeight);
				const xValue = calcValue(x, window.innerWidth);

				this.element.style.transform = `rotateX(${yValue}deg) rotateY(${xValue}deg)`;
			});

		});

	}
}