class ServiceItems {
	constructor(containers) {
		this.containers = containers;
		this.items = [];

		this.init();
	}

	init() {

		this.containers.forEach( (item) => {
			this.items.push( new ServiceItem(item) );
		} );

	}
}