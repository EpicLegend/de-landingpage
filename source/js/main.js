//= components/menu.js
//= components/btnToTop
//= components/serviceItem
//= components/serviceItems

class Site {
	constructor(){
		this.menu;
		this.btnToTop;
		this.serviceItems;
	}

	init() {
		this.menu = new Menu(document.querySelector("#burger-btn"), document.querySelector("#site-menu"));
		this.btnToTop = new BtnToTop(document.querySelector(".btn-to-top"));
		this.serviceItems = new ServiceItems( document.querySelectorAll(".service__container-animation") );
	}
}

window.addEventListener("DOMContentLoaded", function() {
	const site = new Site();
	site.init();
});
